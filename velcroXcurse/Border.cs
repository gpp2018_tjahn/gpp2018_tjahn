﻿using System;
using SFML.System;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Shared;
using Microsoft.Xna.Framework;
using VelcroPhysics.Factories;
using VelcroPhysics.Collision.Filtering;

namespace velcroXcurse
{
    internal class Border
    {
        private Body _anchor;

        public Border(World world, Vector2f size)
        {
            Vertices borders = CreateVertices(size);
            _anchor = BodyFactory.CreateLoopShape(world, borders);
            _anchor.Restitution = 1;
        }

        private static Vertices CreateVertices(Vector2f size)
        {
            // Physics
            float hw = 0.5f * size.X;
            float hh = 0.5f * size.Y;

            Vertices borders = new Vertices(4);
            borders.Add(new Vector2(-hw, hh)); // Lower left
            borders.Add(new Vector2(hw, hh)); // Lower right
            borders.Add(new Vector2(hw, -hh)); // Upper right
            borders.Add(new Vector2(-hw, -hh)); // Upper left
            return borders;
        }

        internal void Resize(Vector2f size)
        {
            var old = _anchor.FixtureList[0];
            _anchor.DestroyFixture(old);
            FixtureFactory.AttachLoopShape(CreateVertices(size), _anchor);
        }
    }
}