﻿using SFML.Graphics;

namespace velcroXcurse
{
    internal interface IDrawable
    {
        void Draw(RenderWindow target);
    }
}