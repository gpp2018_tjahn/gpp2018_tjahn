﻿using Microsoft.Xna.Framework;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using VelcroPhysics.Dynamics;

namespace velcroXcurse
{
    class Program
    {
        static RenderWindow _window;
        static Stopwatch _time;
        static World _world;
        static Border _border;
        static Pyramid _boxes;
        static Paddle _paddle;
        static List<IDrawable> _drawables = new List<IDrawable>();

        private static float AspectRatio => _window.Size.X / (float)_window.Size.Y;


        static void Main(string[] args)
        {
            Initialize();

            while (_window.IsOpen)
            {
                _window.DispatchEvents();
                Update();
                Render();
            }
        }

        private static void Initialize()
        {
            _window = new RenderWindow(
                new VideoMode(1600, 900),
                "velcroXcurse",
                Styles.Default,
                new ContextSettings(24, 8, 8));

            _window.Closed += (sender, args) => _window.Close();
            _window.Resized += (sender, args) => Resize(30);

            _world = new World(Vector2.Zero);
            _border = new Border(_world, new Vector2f(AspectRatio * 30, 30));

            _paddle = new DynamicPaddle(_world, new Vector2(0f, -10));
            _drawables.Add(_paddle);

            _boxes = new Pyramid(_world, new Vector2(0f, 15f), 12);
            _drawables.Add(_boxes);

            for (int i = 0; i < 10; i++)
            {
                var ball = new Ball(_world, new Vector2(2*i-10, -10));
                ball.Throw(-15f + i, i);
                _drawables.Add(ball);
            }

            Resize(30); //We want to see a 30m x 30m square around ZERO

            _time = new Stopwatch();
            _time.Start();
        }

        private static void Resize(float sizeInMeter)
        {
            Vector2f center = new Vector2f(0, 0);
            Vector2f size = new Vector2f(AspectRatio * sizeInMeter, sizeInMeter);
            _window.SetView(new View(center, size));
            _border.Resize(size);
        }
        
        private static void Update()
        {
            float dt = Math.Min(1 / 30f, (_time.ElapsedTicks / (float)Stopwatch.Frequency));
            _time.Restart();
            // variable time step but never less then 30 Hz
            _paddle.Update(dt);
            _world.Step(dt);
        }

        private static void Render()
        {
            _window.Clear();
            _boxes.Draw(_window);
            foreach (var drawable in _drawables)
                drawable.Draw(_window);
            _window.Display();
        }
    }
}
