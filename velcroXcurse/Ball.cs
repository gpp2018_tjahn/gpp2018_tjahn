﻿using System;
using Microsoft.Xna.Framework;
using SFML.Graphics;
using SFML.System;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Factories;

namespace velcroXcurse
{
    internal class Ball : IDrawable
    {
        const float RADIUS = 1f;

        private readonly Sprite _sprite;
        private readonly Body _body;

        public Ball(World world, Vector2 pos)
        {
            var shape = new VelcroPhysics.Collision.Shapes.CircleShape(RADIUS, 1f);
            _body = BodyFactory.CreateBody(world, pos, 0, BodyType.Dynamic);
            _body.CreateFixture(shape);
            _body.Restitution = 1;
            _body.Friction = 0.25f;

            Texture texture = new Texture("Assets/ball.png");
            _sprite = new Sprite(texture);
            _sprite.Origin = new Vector2f(0.5f * texture.Size.X, 0.5f * texture.Size.Y);
            
            _sprite.Scale = new Vector2f(2*RADIUS / texture.Size.X, 2 * RADIUS / texture.Size.Y);
        }

        public void Draw(RenderWindow target)
        {
            _sprite.Position = _body.Position.ToV2f();
            _sprite.Rotation = MathHelper.ToDegrees(_body.Rotation);
            target.Draw(_sprite);
        }

        public void Throw(float vx, float vy)
        {
            Vector2 velocity = new Vector2(vx, vy);
            _body.LinearVelocity = velocity;
        }
        
    }
}