﻿using Microsoft.Xna.Framework;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using VelcroPhysics.Collision.Filtering;
using VelcroPhysics.Collision.Shapes;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Factories;
using VelcroPhysics.Shared;
using VelcroPhysics.Utilities;

namespace velcroXcurse
{
    internal abstract class Paddle : IDrawable
    {
        const float WIDTH = 4;
        const float HEIGHT = 1f;

        private readonly Sprite _sprite;
        protected readonly Body _body;

        public Paddle(World world, Vector2 pos, BodyType type)
        {
            Vertices rect = PolygonUtils.CreateRectangle(0.5f * WIDTH, 0.5f * HEIGHT);
            PolygonShape shape = new PolygonShape(rect, 2);

            _body = BodyFactory.CreateBody(world, pos, 0, type);
            _body.CreateFixture(shape);

            Texture texture = new Texture("Assets/paddle.png");
            _sprite = new Sprite(texture);
            _sprite.Origin = new Vector2f(0.5f * texture.Size.X, 0.5f * texture.Size.Y);

            _sprite.Scale = new Vector2f(WIDTH / texture.Size.X, HEIGHT / texture.Size.Y);
        }

        public void Draw(RenderWindow target)
        {
            _sprite.Position = _body.Position.ToV2f();
            _sprite.Rotation = MathHelper.ToDegrees(_body.Rotation);
            target.Draw(_sprite);
        }

        protected Vector2 GetInputAxis()
        {
            var v = new Vector2();
            if (Keyboard.IsKeyPressed(Keyboard.Key.Left))
                v -= Vector2.UnitX;
            if (Keyboard.IsKeyPressed(Keyboard.Key.Right))
                v += Vector2.UnitX;

            if (Keyboard.IsKeyPressed(Keyboard.Key.Up))
                v -= Vector2.UnitY;
            if (Keyboard.IsKeyPressed(Keyboard.Key.Down))
                v += Vector2.UnitY;

            return v;
        }

        public abstract void Update(float dt);
    }

    internal class KinematicPaddle : Paddle
    {
        const float SPEED = 16f;
        
        public KinematicPaddle(World world, Vector2 pos) : base(world, pos, BodyType.Kinematic)
        {
        }        

        public override void Update(float dt)
        {
            _body.LinearVelocity = GetInputAxis() * SPEED;
        }
    }
    
    internal class DynamicPaddle : Paddle
    {
        const float FORCE = 600f;
        const float TORQUE = 500f;

        public DynamicPaddle(World world, Vector2 pos) : base(world, pos, BodyType.Dynamic)
        {
            _body.AngularDamping = 5f;
            _body.LinearDamping = 5f;
        }  

        public override void Update(float dt)
        {
            _body.ApplyForce(GetInputAxis() * FORCE);
            _body.ApplyTorque(-_body.Rotation * TORQUE);
        }
    }
}