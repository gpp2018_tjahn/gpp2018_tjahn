﻿using Microsoft.Xna.Framework;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace velcroXcurse
{
    static class Extensions
    {
        public static Vector2f ToV2f(this Vector2 vector)
        {
            return new Vector2f(vector.X, vector.Y);
        }
    }
}
