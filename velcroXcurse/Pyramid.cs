﻿using Microsoft.Xna.Framework;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using VelcroPhysics.Collision.Shapes;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Factories;
using VelcroPhysics.Shared;
using VelcroPhysics.Utilities;
using VelcroPhysics.Collision.ContactSystem;

namespace velcroXcurse
{
    public class Pyramid : IDrawable
    {
        const float HALF_WIDTH = 0.6f;
        const float HALF_HEIGHT = 0.5f;

        private readonly Sprite _box;
        private readonly List<Body> _boxes;

        public Pyramid(World world, Vector2 position, int count)
        {
            Vertices rect = PolygonUtils.CreateRectangle(HALF_WIDTH, HALF_HEIGHT);
            PolygonShape shape = new PolygonShape(rect, 1f);

            Vector2 rowStart = position;
            rowStart.Y -= 0.5f + count * 1.1f;

            Vector2 deltaRow = new Vector2(-0.625f, 1.1f);
            const float spacing = 1.25f;

            // Physics
            _boxes = new List<Body>();

            for (int i = 0; i < count; i++)
            {
                Vector2 pos = rowStart;

                for (int j = 0; j < i + 1; j++)
                {
                    Body body = BodyFactory.CreateBody(world);
                    body.BodyType = BodyType.Static;
                    body.Position = pos;

                    var fixture = body.CreateFixture(shape);
                    fixture.OnCollision += (a, b, contact) => HandleCollision(body, world);

                    body.Restitution = 1;
                    _boxes.Add(body);

                    pos.X += spacing;
                }
                rowStart += deltaRow;
            }

            //GFX
            Texture texture = new Texture("Assets/crate.png");
            _box = new Sprite(texture);
            _box.Origin = new Vector2f(0.5f * texture.Size.X, 0.5f * texture.Size.Y);
            var aabb = rect.GetAABB();
            _box.Scale = new Vector2f(aabb.Width / texture.Size.X, aabb.Height / texture.Size.Y);
        }

        private void HandleCollision(Body body, World world)
        {
            world.RemoveBody(body);
            _boxes.Remove(body);
        }

        public void Draw(RenderWindow target)
        {
            foreach (Body body in _boxes)
            {
                _box.Position = body.Position.ToV2f();
                target.Draw(_box);
            }
        }
    }
}